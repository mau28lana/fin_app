import 'package:fin_app/features/root/data/models/report_models.dart';
import 'package:tabler/tabler.dart';

class DebugFunction {
  TablerStyle tablerStyle = TablerStyle(
    verticalChar: '|',
    horizontalChar: "=",
    junctionChar: "#",
    align: TableTextAlign.center,
  );

  void reportsModelDebug(ReportsModel reportsModel, {String? debugTitle}) {
    final tableReportsData = Tabler(
      header: ['Field', 'Value'],
      data: [
        ['Reports ID', reportsModel.reportsId ?? ''],
        ['Date Published', reportsModel.datePublished?.toString() ?? ''],
        ['Updated At', reportsModel.updatedAt?.toString() ?? ''],
      ],
      style: tablerStyle,
    );

    if (reportsModel.userData != null) {
      tableReportsData.add(['User ID', reportsModel.userData!.userId ?? '']);
      tableReportsData.add(['Username', reportsModel.userData!.username ?? '']);
      tableReportsData.add(['Jabatan', reportsModel.userData!.jabatan ?? '']);
    }

    if (reportsModel.reportsData != null) {
      tableReportsData.add(['Campus', reportsModel.reportsData!.campus ?? '']);
      tableReportsData
          .add(['Location', reportsModel.reportsData!.location ?? '']);
      tableReportsData.add(['Status', reportsModel.reportsData!.status ?? '']);
      tableReportsData.add([
        'Reports Description',
        reportsModel.reportsData!.reportsDescription ?? ''
      ]);
      tableReportsData.add([
        'Fixed Description',
        reportsModel.reportsData!.fixedDescription ?? ''
      ]);
    }
    if (debugTitle != null) {
      print(debugTitle);
    }
    print(tableReportsData.toString());
    var videoUrl = reportsModel.mediaUrl?.videoUrl;
    var imageUrls = reportsModel.mediaUrl?.imageUrls;
    var fixedVideoUrl = reportsModel.mediaUrl?.fixedVideoUrl;
    var fixedImageUrls = reportsModel.mediaUrl?.fixedImageUrls;
    print('Media URL Data');
    if (imageUrls != null) {
      print("Image URL 1: ${imageUrls[0]}");
      if (imageUrls.length > 1) {
        print("Image URL 2: ${imageUrls[1]}");
        if (imageUrls.length > 2) {
          print("Image URL 3: ${imageUrls[2]}");
        }
      }
    }
    if (videoUrl != null || videoUrl == "") {
      print("Video URL: ${videoUrl ?? "null"}");
    }
    if (fixedImageUrls != null) {
      print("Fixed Image URL 1: ${fixedImageUrls[0] ?? "null"}");
      if (fixedImageUrls.length > 1) {
        print("Fixed Image URL 2: ${fixedImageUrls[1]}");
        if (fixedImageUrls.length > 2) {
          print("Fixed Image URL 3: ${fixedImageUrls[2]}");
        }
      }
    }
    if (fixedVideoUrl != null || fixedVideoUrl == "") {
      print("Fixed Video URL: ${fixedVideoUrl ?? "null"}");
    }
  }

  Future<void> listReportsModelDebug(
      List<ReportsModel> listReportsModel, String role,
      {String? username, String? campus}) async {
    try {
      final List<ReportsModel> listReportsData = listReportsModel;

      if (role == "all" || role == "admin") {
        print('Semua Data Laporan:');
      } else if (role == "pelapor") {
        print('Laporan yang dibuat oleh $username');
      } else if (role == "teknisi") {
        print('Laporan Di $campus');
      } else {
        throw ArgumentError("Invalid Role");
      }
      for (final reportsModel in listReportsData) {
        reportsModelDebug(reportsModel);
      }
    } catch (e) {
      print('Error fetching ReportsModels: $e');
    }
  }
}
