import 'package:fin_app/constant/color.dart';
import 'package:fin_app/features/auth/data/localresources/auth_local_storage.dart';
import 'package:fin_app/features/root/bloc/root_bloc.dart';
import 'package:fin_app/features/root/components/custom_button.dart';
import 'package:fin_app/features/root/data/models/report_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class TicketList extends StatefulWidget {
  final RootBloc rootBloc;
  const TicketList({Key? key, required this.rootBloc}) : super(key: key);

  @override
  State<TicketList> createState() => _TicketListState();
}

class _TicketListState extends State<TicketList> {
  String? currentRole;

  Future<void> initRoleData() async {
    await getRole();
    fetchData();
  }

  Future<void> getRole() async {
    final role = await AuthLocalStorage().getRole();
    setState(() {
      currentRole = role;
    });
  }

  Future<void> fetchData() async {
    void getReportsForRole(String role, {String? campus}) {
      context
          .read<RootBloc>()
          .add(GetReportsEvent(role, "reports", campus: campus));
    }

    switch (currentRole) {
      case "reporter":
        getReportsForRole("reporter");
        break;
      case "krt_kampus_1":
        getReportsForRole("krt", campus: "Kampus 1");
        break;
      case "krt_kampus_2":
        getReportsForRole("krt", campus: "Kampus 2");
        break;
      case "krt_kampus_3":
        getReportsForRole("krt", campus: "Kampus 3");
        break;
      case "krt_kampus_4":
        getReportsForRole("krt", campus: "Kampus 4");
        break;
      case "krt_kampus_5":
        getReportsForRole("krt", campus: "Kampus 5");
        break;
      case "krt_kampus_6":
        getReportsForRole("krt", campus: "Kampus 6");
        break;
      default:
        getReportsForRole("admin");
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    initRoleData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
        icon: Icon(Icons.close, color: Colors.black),
        onPressed: () {
          context.pop();
        },
      )),
      body: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(12),
        child: BlocBuilder<RootBloc, RootState>(
          builder: (context, state) {
            if (state is ReportsState && state.isLoading) {
              return Center(
                child: LoadingAnimationWidget.horizontalRotatingDots(
                  color: AppColors.primaryColor,
                  size: 24,
                ),
              );
            } else if (state is ReportsState &&
                !state.isLoading &&
                !state.isError) {
              final List<ReportsModel> listReportsData =
                  state.reportsData!.toList();
              return ListView.builder(
                itemCount: listReportsData.length,
                itemBuilder: (context, index) {
                  final ReportsModel reportsData = listReportsData[index];
                  return Container(
                    margin: const EdgeInsets.only(bottom: 12),
                    padding: const EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: SizedBox(
                      width: double.infinity,
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(reportsData.reportsId!),
                          CustomButton(
                            width: 24,
                            textButton: "Copy",
                            onTap: () {
                              Clipboard.setData(
                                ClipboardData(text: reportsData.reportsId!),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            }
            return Center(
              child: Text("Error saat mengambil data"),
            );
          },
        ),
      ),
    );
  }
}
